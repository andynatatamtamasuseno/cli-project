const inquirer = require('inquirer');
const path = require('path');
let dir = path.dirname('/var/log');
let result = dir.toString('utf-8');

module.exports = {
    askGithubCredentials: () => {
        const questions = [
            {
                name: 'Help',
                type: 'input',
                message: 'Apakah Anda Perlu bantuan ? (yes/no):',
                validate: function (value) {
                    if (value == 'yes') {
                        console.log('\nProgram ini berfungsi untuk mengambil file log pada file system milik Linux pada folder /var/log.');
                        console.log('Dari file log tersebut pengguna bisa memilih opsi untuk mengkonversi file tersebut menjadi Plaintext atau JSON.\n');
                        return true;
                    } else {
                        return true;
                    }
                }
            },
            {
                name: 'Direct',
                type: 'input',
                message: 'Pilih name file example /var/log/error.log:',
                validate: function (value) {
                    if (value.length) {
                        dir = path.dirname(value);
                        if(dir == null){
                            return 'file name not Found!'
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            {
                name: 'Konversi',
                type: 'input',
                message: 'Pilih Konversi ( Plaintext || JSON ):',
                validate: function (value) {
                    if (value == 'JSON') {
                        console.log('ini dir', dir);
                        result = JSON.stringify(dir);
                        console.log(result);
                        return true;
                    } else {
                        result = dir.toString('utf-8')
                        console.log(result);
                        return true;
                    }
                }
            },
            {
                name: 'Output',
                type: 'input',
                message: 'Pilih Directorate Output (pilih "no" untuk penyimpanan default):',
                validate: function (value) {
                    if (value == 'No') {
                        return true;
                    } else {
                        console.log('\n' + value)
                        return true;
                    }
                }
            },
        ];
        return inquirer.prompt(
            questions)
    },
};
